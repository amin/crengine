PROJECT(hblib)
cmake_minimum_required(VERSION 2.8)
enable_language(C CXX)

SET(HB_DIR ${CMAKE_CURRENT_SOURCE_DIR}/harfbuzz/src)
SET(UCDN_DIR ${HB_DIR}/hb-ucdn)
SET(FREETYPE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../freetype)

INCLUDE_DIRECTORIES(
    #${CMAKE_CURRENT_SOURCE_DIR}/harfbuzz
    ${HB_DIR}
    ${HB_DIR}/hb-ucdn
    ${CMAKE_CURRENT_SOURCE_DIR}/include
	${FREETYPE_DIR}/include
)

set (CMAKE_C_FLAGS "-g -O2 -MD -MP -fPIC -DPIC")
set (CMAKE_CXX_FLAGS "-pthread -g -O2 -fno-rtti -fno-exceptions -Wcast-align -fvisibility-inlines-hidden -MD -MP  -fPIC -DPIC")

SET (UCDN_SOURCES
    ${UCDN_DIR}/ucdn.c
)

ADD_LIBRARY(hb_ucdn ${UCDN_SOURCES})


message("Will build harfbuzz library")
#ADD_DEFINITIONS(-DHAVE_CONFIG_H)

## This is conifg.h; WIP. generate with cmake CHECK_INCLUDE_FILES
ADD_DEFINITIONS(-DALIGNOF_STRUCT_CHAR__=1)
ADD_DEFINITIONS(-DHAVE_ATEXIT=1)
ADD_DEFINITIONS(-DHAVE_DLFCN_H=1)
ADD_DEFINITIONS(-DHAVE_FALLBACK=1)
ADD_DEFINITIONS(-DHAVE_FONTCONFIG=1)
ADD_DEFINITIONS(-DHAVE_FREETYPE=1)
ADD_DEFINITIONS(-DHAVE_GETPAGESIZE=1)
ADD_DEFINITIONS(-DHAVE_INTEL_ATOMIC_PRIMITIVES=1)
ADD_DEFINITIONS(-DHAVE_INTTYPES_H=1)
ADD_DEFINITIONS(-DHAVE_ISATTY=1)
ADD_DEFINITIONS(-DHAVE_MEMORY_H=1)
ADD_DEFINITIONS(-DHAVE_MMAP=1)
ADD_DEFINITIONS(-DHAVE_MPROTECT=1)
ADD_DEFINITIONS(-DHAVE_OT=1)
ADD_DEFINITIONS(-DHAVE_PTHREAD=1)
ADD_DEFINITIONS(-DHAVE_PTHREAD_PRIO_INHERIT=1)
ADD_DEFINITIONS(-DHAVE_STDINT_H=1)
ADD_DEFINITIONS(-DHAVE_STDLIB_H=1)
ADD_DEFINITIONS(-DHAVE_STRINGS_H=1)
ADD_DEFINITIONS(-DHAVE_STRING_H=1)
ADD_DEFINITIONS(-DHAVE_SYSCONF=1)
ADD_DEFINITIONS(-DHAVE_SYS_MMAN_H=1)
ADD_DEFINITIONS(-DHAVE_SYS_STAT_H=1)
ADD_DEFINITIONS(-DHAVE_SYS_TYPES_H=1)
ADD_DEFINITIONS(-DHAVE_UCDN=1)
ADD_DEFINITIONS(-DHAVE_UNISTD_H=1)

#ADD_DEFINITIONS(-DHB_DEBUG_SHAPE_PLAN=1)

#define LT_OBJDIR ".libs/"
#define PACKAGE_BUGREPORT "http://bugs.freedesktop.org/enter_bug.cgi?product=harfbuzz"
#define PACKAGE_NAME "HarfBuzz"
#define PACKAGE_STRING "HarfBuzz 0.9.40"
#define PACKAGE_TARNAME "harfbuzz"
#define PACKAGE_URL "http://harfbuzz.org/"
#define PACKAGE_VERSION "0.9.40"
#define STDC_HEADERS 1

#ADD_DEFINITIONS(-DHAVE_OT)
#ADD_DEFINITIONS(-DHAVE_UCDN)
#ADD_DEFINITIONS(-DHB_NO_MT)
#ADD_DEFINITIONS(-DHAVE_INTEL_ATOMIC_PRIMITIVES)

SET (HB_SOURCES
    ${HB_DIR}/hb-blob.cc
    ${HB_DIR}/hb-buffer-serialize.cc
    ${HB_DIR}/hb-buffer.cc
    ${HB_DIR}/hb-common.cc
    ${HB_DIR}/hb-face.cc
    ${HB_DIR}/hb-font.cc
    ${HB_DIR}/hb-ot-tag.cc
    ${HB_DIR}/hb-set.cc
    ${HB_DIR}/hb-shape.cc
    ${HB_DIR}/hb-shape-plan.cc
    ${HB_DIR}/hb-shaper.cc
    ${HB_DIR}/hb-unicode.cc
    ${HB_DIR}/hb-warning.cc
    ${HB_DIR}/hb-ot-font.cc
    ${HB_DIR}/hb-ot-layout.cc
    ${HB_DIR}/hb-ot-map.cc
    ${HB_DIR}/hb-ot-shape.cc
    ${HB_DIR}/hb-ot-shape-complex-arabic.cc
    ${HB_DIR}/hb-ot-shape-complex-default.cc
    ${HB_DIR}/hb-ot-shape-complex-hangul.cc
    ${HB_DIR}/hb-ot-shape-complex-hebrew.cc
    ${HB_DIR}/hb-ot-shape-complex-indic.cc
    ${HB_DIR}/hb-ot-shape-complex-indic-table.cc
    ${HB_DIR}/hb-ot-shape-complex-myanmar.cc
    ${HB_DIR}/hb-ot-shape-complex-sea.cc
    ${HB_DIR}/hb-ot-shape-complex-thai.cc
    ${HB_DIR}/hb-ot-shape-complex-tibetan.cc
    ${HB_DIR}/hb-ot-shape-normalize.cc
    ${HB_DIR}/hb-ot-shape-fallback.cc
    ${HB_DIR}/hb-fallback-shape.cc
    ${HB_DIR}/hb-ft.cc
    ${HB_DIR}/hb-ucdn.cc
)
#ADD_LIBRARY(harfbuzz SHARED ${HB_SOURCES})
ADD_LIBRARY(harfbuzz ${HB_SOURCES})

target_link_libraries(harfbuzz hb_ucdn)

